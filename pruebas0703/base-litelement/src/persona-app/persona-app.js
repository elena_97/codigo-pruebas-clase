 import {LitElement, html} from 'lit';
 import '../persona-header/persona-header.js';
 import '../persona-main/persona-main.js';
 import '../persona-footer/persona-footer.js';
 import '../persona-sidebar/persona-sidebar.js';
 import '../persona-form/persona-form.js';


 class PersonaApp extends LitElement{

    static get properties() {
        return {
        };
    }

    constructor(){
        super();
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class="col-1" @new-person="${this.newPerson}"></persona-sidebar>
                <persona-main class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
        `
    }

    newPerson(e) {
        console.log("newPerson en persona-app");

        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

 }

 customElements.define("persona-app", PersonaApp);